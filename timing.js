function Clock (){
	this.currentTime = null;
}

Clock.TICK = 5000;

Clock.prototype.printTime = function () {
  console.log([
    this.currentTime.getHours(),
    this.currentTime.getMinutes(),
    this.currentTime.getSeconds()
  ].join(":"));
};

Clock.prototype.run = function () {
	this.currentTime = new Date();
	this.printTime();
	
	setInterval(this._tick.bind(this), Clock.TICK);
};

Clock.prototype._tick = function () {
	this.currentTime = new Date(this.currentTime.getTime() + Clock.TICK);
	this.printTime();
};

clock = new Clock();
clock.run();