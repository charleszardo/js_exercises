var readline = require('readline');

var reader = readline.createInterface({
	input: process.stdin,
	output: process.stdout
})

var addNumbers = function (sum, numsLeft, completionCallback){
	if (numsLeft > 0){
		reader.question("Gimme a number ", function (num) {
			var num = parseInt(num);
			sum += num;
			numsLeft -= 1
			console.log("Sum so far: " + sum);
			
			addNumbers(sum, numsLeft, completionCallback);
		});
	} else {
		completionCallback(sum);
	}
}

addNumbers(0, 3, function (sum) {
  console.log("Total Sum: " + sum);
  reader.close();
});